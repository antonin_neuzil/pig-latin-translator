<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;


final class HomepagePresenter extends Nette\Application\UI\Presenter
{

    private $pigLatinText;
    private $originText;
    private $suffixAy = "ay";
    private $exceptionPrefix = ["qu", "squ"];
    private $vowels = 'aeiouAEIOU';


    public function renderDefault(){
        if ($this->pigLatinText === null) {
            $this->pigLatinText = 'default text';
        }
        $this->template->pigLatinText = $this->pigLatinText;
    }

    protected function createComponentTranslationForm(){
        $form = new Form;
        $form->addTextArea('input', "Text to translation:")
            ->setRequired()
            ->addRule($form::MAX_LENGTH, 'Text is too long.', 10000)
            ->setHtmlAttribute('class', 'text_field');
        $form->addSubmit('submit', 'Translate')
            ->setHtmlAttribute('class', 'submit');
        $form->onSuccess[] = [$this, 'translationFormSubmitted'];
        return $form;
    }

    public function translationFormSubmitted(Form $form, $data){
        $this->originText = $data->input;
        $this->translationAlgorithm();
        $this->redrawControl('pigLatinTextSnippet');
    }

    private function translationAlgorithm(){
        $this->pigLatinText = "";
        while ($this->originText != ''){
            if ($this->isLetter(substr($this->originText, 0, 1))){
                $word = $this->extractFirstWord();
                $this->pigLatinText .= $this->transformWord($word);
            }else{
                $this->copyNotLetters();
            }
        }
    }

    private function extractFirstWord(){
        preg_match('/\w+/', $this->originText, $output_array);
        $firstWord = $output_array[0];
        $this->originText = substr($this->originText, strlen($firstWord));
        return $firstWord;
    }

    private function copyNotLetters(){
        while ($this->originText != "" && !$this->isLetter(substr($this->originText, 0, 1))){
            $this->pigLatinText .= substr($this->originText, 0, 1);
            $this->originText = substr($this->originText, 1);
        }
    }

    private function isLetter($char){
        return preg_match("/^[a-zA-Z]$/", $char);
    }

    private function transformWord($word){
        foreach ($this->exceptionPrefix as $prefix){
            if (strpos($word, $prefix) === 0){
                return substr($word, strlen($prefix)) . $prefix . $this->suffixAy;
            }
        }
        $countOfChars = 0;
        while (strpos($this->vowels, $word[0]) === false){
            $letter = strtolower($word[0]);
            $lengthOneChar = 1;
            $word = substr($word, $lengthOneChar) . $letter;
            $countOfChars += 1;
            if ($countOfChars == strlen($word)){
                break;
            }
        }
        return $word . $this->suffixAy;
    }
}
